import React, { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import ProductList from "./pages/ProductList/ProductList.jsx";
import Header from "./components/Header/Header.jsx";
import Footer from "./components/Footer/Footer.jsx";
import Cart from "./pages/Cart/Cart.jsx";
import Favorites from "./pages/Favorites/Favorites.jsx";
import NotFound from "./pages/NotFound/NotFound.jsx";
import Forma from "./components/Form/Forma.jsx";

import { actionFetchProducts } from "./store/actions.js";

import "./App.css";


function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actionFetchProducts());
  }, []);

  const cart = useSelector((state) => state.cart);
  const totalPrice = useSelector((state) => state.totalPrice);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
    localStorage.setItem("totalPrice", JSON.stringify(totalPrice));
  }, [cart, totalPrice]);


  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<ProductList />} />
        <Route path="/cart" element={<Cart totalPrice={totalPrice} />} />
        <Route path="/favorites" element={<Favorites />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
      <Footer />
    </>
  );
}

export default App;
