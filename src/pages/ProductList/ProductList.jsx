import React from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

import ProductItem from "../../components/ProductItem/ProductItem";

import "./ProductList.scss";

function ProductList() {

  const products = useSelector((state) => state.products);
  const favorites = useSelector((state) => state.favorites);
  const cart = useSelector((state) => state.cart);

  return (
    <div className="container-list">
      {products.map((obj) => (
        <div className="wrapper" key={obj.id}>
          <ProductItem
            isFavorite={favorites.some(({ id }) => obj.id === id)}
            isCart={cart.some(({ id }) => obj.id === id)}
            name={obj.name}
            price={obj.price}
            imgUrl={obj.imgUrl}
            id={obj.id}
            color={obj.color}
          />
        </div>
      ))}
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.array,
};

export default ProductList;
