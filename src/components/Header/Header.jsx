import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

import { ReactComponent as Star } from "../../assets/icons/starIcon.svg";
import { ReactComponent as CartLogo } from "../../assets/icons/cart.svg";
import { ReactComponent as Logo } from "../../assets/icons/logo.svg";

import "./Header.scss";

function Header() {

  const favorites = useSelector((state) => state.favorites)
  const cart = useSelector((state) => state.cart)

  return (
    <>
      <div className="root">
        <div className="container">
          <div>
            <Link to="/">
              <Logo className="logo" />
            </Link>
          </div>
          <div className="wrap">
            <Link className="header-icon" to="/favorites">
              <Star /><span>{favorites.length}</span> 
            </Link>
            <Link className="header-icon" to="/cart">
              <CartLogo /><span>{cart.length}</span>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}

Header.propTypes = {
  favorites: PropTypes.array,
  cart: PropTypes.array,
};

export default Header;
